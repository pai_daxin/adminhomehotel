import request from "@/utils/request";

//根据条件分页查询
export const findAdminPageAPI = (data) => {
	return request({
		url: `/manager/list`,
		method: "post",
		data,
	});
};

//新增
export const addAdminAPI = (data) => {
	return request({
		url: "/manager/save",
		method: "post",
		data,
	});
};

//修改
export const modifyAdminAPI = (data) => {
	return request({
		url: "/manager/",
		method: "put",
		data,
	});
};

//删除
export const removeAdminAPI = (id) => {
	return request({
		url: `/manage/${id}`,
		method: "delete",
	});
};
