import request from "@/utils/request";

//根据条件分页查询
export const findMemberPageAPI = (data) => {
	return request({
		url: `/user/list`,
		method: "post",
		data,
	});
};

//根据条件查询
export const findMemberAPI = (data) => {
	return request({
		url: "/user/list",
		method: "get",
		data,
	});
};

//查询全部
export const findAllMemberAPI = (data) => {
	return request({
		url: "/user/list",
		method: "get",
		data,
	});
};

//新增
export const addMemberAPI = (data) => {
	return request({
		url: "/user/save",
		method: "post",
		data,
	});
};

//修改
export const modifyMemberAPI = (data) => {
	return request({
		url: "/user/edit",
		method: "put",
		data,
	});
};

//删除
export const removeMemberAPI = (id) => {
	return request({
		url: `/user/${id}`,
		method: "delete",
	});
};
